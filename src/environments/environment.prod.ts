
const baseUrl = 'https://evento-digital-api.herokuapp.com';

export const environment = {
  production: true,
  baseUrl
};

