import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Evento } from './evento';

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  private apiUrl: string = environment.baseUrl + '/evento';

  constructor(private http: HttpClient) { }

  getEventos(plataforma: string): Observable<Evento[]> {

    return this.http.get<Evento[]>(this.apiUrl + '?plataforma=' + plataforma);

  }
}
