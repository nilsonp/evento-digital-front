import { Component, OnInit } from '@angular/core';
import { Evento } from './evento';
import { EventoService } from './evento.service';
import { Plataformas } from './plataformas';

interface PlatListInterface {
  id: Plataformas;
  name: string;
}

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {

  eventos: Array<Evento> = [];
  plataforma: Plataformas = Plataformas.PACTO;
  platList: PlatListInterface[];


  constructor( private eventoService: EventoService) { 

    this.platList = [
        {id: Plataformas.PACTO, name: "Plataforma de generación de pagarés"}, 
        {id: Plataformas.DEPO, name: "Plataforma de bóveda de valores"}, 
        {id: Plataformas.BAKO, name: "Plataforma de backoffice"}, 
        {id: Plataformas.BAKO, name: "Plataforma digital"}
      ];
  }

  getEventos(): void {
    this.eventoService.getEventos(this.plataforma.toString()).subscribe( (ev) => { 
      this.eventos = ev;
      console.log(ev.length);
    });
  }

  ngOnInit(): void {
    this.getEventos(); 
  }

}
