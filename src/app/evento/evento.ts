export class Evento {

    plataforma: string;
    idEvento: string;
    numeroEventos: number;
    fechaEvento: Date;
    costoEvento: number;

    constructor(
        plataforma: string,
        idEvento: string,
        numeroEventos: number,
        fechaEvento: Date,
        costoEvento: number
    ){
        this.plataforma = plataforma;
        this.idEvento = idEvento;
        this.numeroEventos = numeroEventos;
        this.fechaEvento = fechaEvento;
        this.costoEvento = costoEvento;
    }

}
